var UserViewModel = require("../../shared/view-models/user-view-model");
var frameModule = require("ui/frame");
var config = require("../../shared/config");
var fetchModule = require("fetch");
var dialogsModule = require("ui/dialogs");

var user = new UserViewModel({
    email: "correo@prueba.com",
    password: "123456"
});

var page;
var email;

exports.loaded = function(args) {
    page = args.object;
    if (page.ios) {
        var navigationBar = frameModule.topmost().ios.controller.navigationBar;
        navigationBar.barStyle = UIBarStyle.UIBarStyleBlack;
    }
    page.bindingContext = user;
};

exports.signIn = function() {
    user.login()
        .catch(function(error) {
            console.log(error);
            dialogsModule.alert({
                message: "Desafortunadamente no hemos podido encontrar tu cuenta.",
                okButtonText: "Aceptar"
            });
            return Promise.reject();
        })
        .then(function() {
            frameModule.topmost().navigate("views/list/list");
        });
};

exports.register = function() {
    var topmost = frameModule.topmost();
    topmost.navigate("views/register/register");
};